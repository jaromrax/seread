#!/usr/bin/env python3

from fire import Fire
import serial, time
SERIALPORT = "/dev/ttyUSB0"
BAUDRATE = 9600

def main():
    print(f"i... ok")
    ser = serial.Serial(SERIALPORT, BAUDRATE)
    ser.bytesize = serial.EIGHTBITS
    ser.parity = serial.PARITY_NONE
    ser.stopbits = serial.STOPBITS_ONE #n
    ser.timeout = 2              #timeout block read
    ser.flushInput()
    ser.flushOutput()

    ser.write(b"ds\r\n")
    res = ser.readline()
    while len(res.decode('utf8'))>1:
        print(res.decode("utf8"))
        res = ser.readline()


if __name__=="__main__":
    Fire(main)
